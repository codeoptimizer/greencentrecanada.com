<?php
    /*
      Plugin Name: Custom Site Search
      Plugin URI:
      description: Custom script for site search
      Version: 1.0
      Author: 1dea
      Author URI: https://1dea.ca/
      License: GPL2
      */

    function advance_search_theme_js_script() {
        wp_enqueue_style(
            'advance-search-style',
            plugins_url( '', __FILE__ ).'/css/' . 'style.css',
            false,
            '1.10.4',
            'all'
        );
        wp_enqueue_script('advance-search-functions',  plugins_url( '', __FILE__ ).'/js/'.'functions.js', array(), false, true );
        wp_localize_script( 'advance-search-functions', 'ajax_posts_filter', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
        ));

    }
    add_action('wp_enqueue_scripts', 'advance_search_theme_js_script');

    function advance_search_admin_script_style( $hook ) {
        if ( 'post.php' == $hook || 'post-new.php' == $hook ) {
            wp_enqueue_script(
                'advance-search-functions',
                plugins_url( '', __FILE__ ).'/js/' . 'functions.js',
                array( 'jquery', 'jquery-functions' ),
                '1.0',
                true
            );

            wp_enqueue_style(
                'advance-search-style',
                plugins_url( '', __FILE__ ).'/css/' . 'style.css',
                false,
                '1.10.4',
                'all'
            );
        }
    }
    add_action( 'admin_enqueue_scripts', 'advance_search_admin_script_style' );

    function advance_search_posts_topics() {
        ob_start();
        ?>
        <?php
        $args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
        $categories = get_categories($args );
        ?>
        <select class="advance-search-select-topic">
            <option value="">Topic</option>
            <?php
                if(!empty( $categories)) {
                    $separator = ' ';
                    $output = '';

                    foreach( $categories as $category ) {
                        $cat_obj = get_term($term->term_id, 'category');
                        ?>
                        <option value="<?php echo $category->term_id;?>"  <?php if($_GET['topic-id']==$category->term_id) echo 'selected' ?> ><?php echo esc_html( $category->name ); ?></option>
                        <?php
                    }
                }
            ?>
        </select>
        <?php
        $content = ob_get_clean();
        return $content;
    }
    add_shortcode('advance_search_topics', 'advance_search_posts_topics');

    function advance_search_posts_author() {
        ob_start();
        ?>
        <?php
        $args  = array(
            // search only for Authors role
            'role__in' => array( 'Author','Administrator'),
            'orderby' => 'display_name'
        );

        $wp_user_query = new WP_User_Query($args);
        $authors = $wp_user_query->get_results();
        ?>
        <select class="advance-search-select-author">
            <option value="">Author</option>
            <?php
                // loop trough each author
                foreach ($authors as $author)
                {
                    $author_info = get_userdata($author->ID);
                    ?>
                    <option value="<?php echo $author->ID;?>"><?php echo esc_html( $author_info->first_name.' '.$author_info->last_name ); ?></option>
                    <?php
                }
            ?>
        </select>
        <?php
        $content = ob_get_clean();
        return $content;
    }
    add_shortcode('advance_search_author', 'advance_search_posts_author');

    function advance_search_posts_year() {
        ob_start();
        ?>
        <?php
        $terms_year = array(
            'post_type'         => array('post'),
        );

        $query_year = new WP_Query( $terms_year );
        $years = array();
        if ( $query_year->have_posts() ) :
            while ( $query_year->have_posts() ) : $query_year->the_post();
                $year = get_the_date('Y');
                if(!in_array($year, $years)){
                    $years[] = $year;

                }
            endwhile;
            wp_reset_postdata();
        endif;

        ?>
        <select class="advance-search-select-year">
            <option value="">Year</option>
            <?php
                foreach($years as $year){
                    ?>
                    <option value="<?php echo $year;?>"><?php echo $year; ?></option>
                    <?php
                }
            ?>
        </select>
        <?php
        $content = ob_get_clean();
        return $content;
    }
    add_shortcode('advance_search_year', 'advance_search_posts_year');

    function advance_search_posts_keyword() {
        ob_start();
        ?>
        <div class="search-box">
            <input placeholder="Search keyword" class="advance-search-keyword" type="search" name="s" title="Search" value="">
            <button class="advance-search-button-search" type="submit" title="Search" aria-label="Search">
                <img src="/wp-content/uploads/2021/09/icon-search-white.svg" >

            </button>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }
    add_shortcode('advance_search_keyword', 'advance_search_posts_keyword');

    function is_FR(){
        return apply_filters( 'wpml_current_language', null ) == 'fr' ? true : false;
    }

    function advance_search_posts() {
        ob_start();

        $total_posts = '';
        $postsPerPage = 12;

        $s=get_search_query();
        $args = array(
            's' =>$s,
            'posts_per_page' => $postsPerPage,
            "post_status" => 'publish',
            'paged' => 1,
            "order" => 'DESC'
        );

        $loop = new WP_Query($args);

        if($loop->found_posts>$postsPerPage){
            $current_page_total = $postsPerPage;
        }else{
            $current_page_total = $loop->found_posts;
        }

        $post_count =0;

        if($_GET['s']){
            $keyword=$_GET['s'];
        }else{
            $keyword='';
        }
        ?>


        <input type="hidden" value="<?php echo $keyword?>" class="search-keyword">

		<?='<h1 id="search_title"><span class="current_search_total_footer">'.$current_page_total.'</span> of <span class="total_search_total_footer">'.$loop->found_posts.'</span> results found for “'.$_GET['s'].'”</h1>';?>

        <div id="advance-search-posts" >
            <?php

                if ( $loop->have_posts() ) {
                    while ($loop->have_posts()) : $loop->the_post();
                        $post_count++;

                        $content = strip_tags(get_the_excerpt());
                        $trimmed_content = wp_trim_words($content, 30, '...');

                        $img_url = '';
                        $post_thumbnail_id = get_post_thumbnail_id();
                        if ( $post_thumbnail_id != '' ) {
                            $img_url = wp_get_attachment_url( $post_thumbnail_id );
                        }

                        //get categories
                        $categories  = get_the_category();
                        $categories_out = array();
                        if(!empty($categories)) {
                            foreach ($categories as $category) {
                                array_push($categories_out, $category->name);
                            }
                        }
                        ?>
                        <div class="advance-search-post-warpper">
                            <div class="advance-search-content-wrapper">
                                <div class="advance-search-content">
                                    <h5 class="advance-search-event-title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h5>
                                    <a href="<?php the_permalink()?>" class="paragraph-link"><div class="advance-search-exceprt"><?php echo $trimmed_content; ?></div></a>
                                    <a href="<?php the_permalink()?>" class="elementor-button"><span>Visit Page</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"><g id="Group_23543" data-name="Group 23543" transform="translate(-115 -1)"><g id="Ellipse_133" data-name="Ellipse 133" transform="translate(115 1)" fill="#fff" stroke="#0f0f0f" stroke-width="1"><circle cx="11" cy="11" r="11" stroke="none"/><circle cx="11" cy="11" r="10.5" fill="none"/></g><g id="arrow-up" transform="translate(129.053 9.426) rotate(90)"><line id="Line_38" data-name="Line 38" y1="7.092" transform="translate(2.58 0.755)" fill="none" stroke="#0f0f0f" stroke-linejoin="round" stroke-width="1"/><path id="Path_5971" data-name="Path 5971" d="M0,2.816,2.575,0,5.15,2.816" fill="none" stroke="#0f0f0f" stroke-width="1"/></g></g></svg>
                                    </a>
                                </div>
                                <?php
                                    if ( $img_url != '' ) {
                                        ?>
                                        <div class="featured-image-section">
                                            <img class="advance-search-img" src="<?php echo $img_url?>" alt=""/>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>
                    <?php
                    endwhile;

                }else{
                    ?>
                    <div class="no-search-result">
                        <h2>Nothing Found</h2>
                        <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
                    </div>
                    <?php
                }

                wp_reset_postdata();
            ?>
        </div>

        <?php if($post_count){ ?>
            <div class="advance-search-total-footer"><?php echo 'Showing <span id="current_search_total_footer">'.$current_page_total.'</span> of <span id="total_search_total_footer">'.$loop->found_posts.'</span>'; ?></div>
            <div class="pagination-bar"><div id="search_progress" style="width:<?php echo ($current_page_total/$loop->found_posts)*100; ?>%;"></div></div>
            <div class="advance-search-load-more-posts elementor-widget secondary">
                <?php if($loop->found_posts>$postsPerPage){ ?>
                    <a href="#" id="search_load_more_posts" data-maxpage="<?php echo $loop->max_num_pages;?>" class="elementor-button">Load More</a>
                <?php }else{ ?>
                    <p class="no_post" >No more posts to show</p>
                    <?php
                }
                ?>
                <p id="no_post" class="no_post" style="visibility:hidden">No more posts to show</p>
            </div>
        <?php }?>
        <?php
        // Output needs to be return
        $content = ob_get_clean();
        return $content;
    }
// register shortcode
    add_shortcode('advance_search', 'advance_search_posts');

    function more_post_filter_ajax(){
        ob_start();
        $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 12;
        $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 2;

        $s=get_search_query();
        $args = array(
            's' =>$s,
            "post_status" => 'publish',
            'suppress_filters' => false,
            'posts_per_page' => $ppp,
            'paged'    => $page,

            "order" => 'DESC'
        );

        if($_POST['keyword']){
            $args['s'] = $_POST['keyword'];
        }

        $loop = new WP_Query($args);

        $out = '';
        $post_count=0;
        if ($loop -> have_posts()){
            header("Content-Type: text/html");

            while ($loop -> have_posts()) : $loop -> the_post();
                $post_count++;

                $content = strip_tags(get_the_excerpt());
                $trimmed_content = wp_trim_words($content, 30, '...');

                $img_url = '';
                $post_thumbnail_id = get_post_thumbnail_id();
                if ( $post_thumbnail_id != '' ) {
                    $img_url = wp_get_attachment_url( $post_thumbnail_id );
                }

                //get categories
                $categories  = get_the_category();
                $categories_out = array();
                if(!empty($categories)) {
                    foreach ($categories as $category) {
                        array_push($categories_out, $category->name);
                    }
                }
                ?>
                <div class="advance-search-post-warpper">
                    <div class="advance-search-content-wrapper">
                        <div class="advance-search-content">
                            <h5 class="advance-search-event-title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h5>
                            <a href="<?php the_permalink()?>" class="paragraph-link"><div class="advance-search-exceprt"><?php echo $trimmed_content; ?></div></a>
                            <a href="<?php the_permalink()?>" class="elementor-button"><span>Visit Page</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"><g id="Group_23543" data-name="Group 23543" transform="translate(-115 -1)"><g id="Ellipse_133" data-name="Ellipse 133" transform="translate(115 1)" fill="#fff" stroke="#0f0f0f" stroke-width="1"><circle cx="11" cy="11" r="11" stroke="none"/><circle cx="11" cy="11" r="10.5" fill="none"/></g><g id="arrow-up" transform="translate(129.053 9.426) rotate(90)"><line id="Line_38" data-name="Line 38" y1="7.092" transform="translate(2.58 0.755)" fill="none" stroke="#0f0f0f" stroke-linejoin="round" stroke-width="1"/><path id="Path_5971" data-name="Path 5971" d="M0,2.816,2.575,0,5.15,2.816" fill="none" stroke="#0f0f0f" stroke-width="1"/></g></g></svg>
                            </a>
                        </div>
                        <?php
                            if ( $img_url != '' ) {
                                ?>
                                <div class="featured-image-section">
                                    <img class="advance-search-img" src="<?php echo $img_url?>" alt=""/>
                                </div>
                                <?php
                            }
                        ?>
                    </div>
                </div>
            <?php
            endwhile;

            wp_reset_postdata();
            $content = ob_get_clean();
            search_counter($post_count);
            die($content);
        }

    }

    add_action('wp_ajax_nopriv_more_post_filter_ajax', 'more_post_filter_ajax');
    add_action('wp_ajax_more_post_filter_ajax', 'more_post_filter_ajax');


    function search_counter($data){
        ?>
        <script>
            update_search_count(<?php echo $data;?>);
        </script>
        <?php
    }


    function remove_load_more_button($page){
        if($page==1){
            ?>
            <script>
                (function( $ ) {
                    $('.advance-search-load-more-posts').hide();
                })( jQuery );
            </script>
            <?php
        }else{
            ?>
            <script>
                (function( $ ) {
                    $('.advance-search-load-more-posts').show();
                })( jQuery );
            </script>
            <?php
        }
    }


    function advance_search_post_search_ajax(){
        ob_start();
        $ppp = 12;
        $page = 0;

        //header("Content-Type: text/html");

        $args = array(
            'post_type' => 'post',
            'suppress_filters' => true,
            'posts_per_page' => $ppp,
            'paged'    => $page,
            "post_status" => 'publish',
            "order" => 'DESC'
        );

        if($_POST['topic']){
            $args['cat'] = $_POST['topic'];
        }

        if($_POST['author']){
            $args['author'] = $_POST['author'];
        }

        if($_POST['year']){
            $args['date_query'] = array( array('year' => $_POST['year']) );
        }

        if($_POST['keyword']){
            $args['s'] = $_POST['keyword'];
        }

        $loop = new WP_Query($args);

        $out = '';
        $col=0;
        if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();

            $col++;
            ?>
            <div class=" advance-search-post-warpper">
                <div class="featured-image"><?php the_post_thumbnail('large'); ?></div>
                <div class="advance-search-content-wrapper">

                    <div class="advance-search-content">
                        <h5 class="advance-search-event-title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h5>
                        <div class="advance-search-exceprt"><?php the_excerpt(); ?></div>
                    </div>

                    <div class="advance_search_readmore"><a href="<?php the_permalink()?>" class="link-arrow-right-blue">READ MORE</a></div>

                </div>
            </div>


            <?php

            if($col==3){  $col = 0; }
        endwhile;
        endif;
        remove_load_more_button($loop->max_num_pages);
        wp_reset_postdata();

        $content = ob_get_clean();
        die($content);
    }

    add_action('wp_ajax_nopriv_advance_search_post_search_ajax', 'advance_search_post_search_ajax');
    add_action('wp_ajax_advance_search_post_search_ajax', 'advance_search_post_search_ajax');


    function search_results_title(){
        ob_start();
//     echo '<h1 id="search_title"><span></span> Results for: “'.$_GET['s'].'”</h1>';
        echo '<h1 id="search_title"><span class="current_search_total_footer">'.$found_posts.'</span> of <span class="total_search_total_footer">'.$total_posts.'</span> results found for “'.$_GET['s'].'”</h1>';
        return ob_get_clean();
    }
    add_shortcode('search_results', 'search_results_title');
?>