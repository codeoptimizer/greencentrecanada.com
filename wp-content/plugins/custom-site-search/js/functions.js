(function( $ ) {
    var ppp = 12; // Post per page
    var pageNumber = 1;

    $("#search_load_more_posts").off().on("click", function(e) {
        e.preventDefault();
        e.stopPropagation();

        if (typeof $('.search-select-topic :selected').val() !== "undefined") {
            var topic = $('.search-select-topic :selected').val();
        }else{
            var topic = '';
        }

        if (typeof $('.search-select-author :selected').val() !== "undefined") {
            var author = $('.search-select-author :selected').val();
        }else{
            var author = '';
        }

        if (typeof $('.search-select-year :selected').val() !== "undefined") {
            var year = $('.search-select-year :selected').val();
        }else{
            var year = '';
        }

        if (typeof $('.search-keyword').val() !== "undefined") {
            var keyword = $('.search-keyword').val();
        }else{
            var keyword = '';
        }

        pageNumber++;
        console.log("Page Number: " + pageNumber);

        if($('#search_load_more_posts').attr("data-maxpage")==pageNumber){
            $("#search_load_more_posts").addClass("disabled");
        }

        var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_post_filter_ajax' + '&keyword=' + keyword ;
        $.ajax({
            type: "POST",
            dataType: "html",
            url: ajax_posts_filter.ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data);

                if($data.length>1){

                    $("#advance-search-posts").append($data);
                } else{
                    $("#search_load_more_posts").hide();
                    $(".search-load-more-posts").append('<div class="no-more-post">No more posts to show</div>');
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
        return false;

    });

    $(".search-button-search").off().on("click", function(e) {
        e.preventDefault();
        e.stopPropagation();

        var topic = $('.search-select-topic :selected').val();
        var author = $('.search-select-author :selected').val();
        var year = $('.search-select-year :selected').val();
        var keyword = $('.search-keyword').val();


        pageNumber=1;

        if($('#search_load_more_posts').attr("data-maxpage")==pageNumber){
            $("#search_load_more_posts").addClass("disabled");
        }
        var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=search_post_search_ajax'+ '&year=' + year + '&topic=' + topic + '&author=' + author + '&keyword=' + keyword ;
        $.ajax({
            type: "POST",
            dataType: "html",
            url: ajax_posts_filter.ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data);

                if($data.length){

                    $("#search-posts").html($data);
                    $("#search_load_more_posts").show();
                    $(".no-more-post").remove();

                } else{
                    $("#search-posts").html('');
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
        return false;
    });


    $(".search-select-topic, .search-select-author, .search-select-year").on("change", function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(".search-button-search").trigger("click");
        return false;
    });

    var url = window.location.href;
    var hash = url.substring(url.indexOf('#'));

    if(hash=='#all-posts'){
        var topic = $('.search-select-topic :selected').val();
        $(".search-button-search").trigger("click");
    }



    $("#reset-filter").off().on("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(".search-select-topic option:contains(Topic)").attr('selected', true);
        $(".search-select-author option:contains(Author)").attr('selected', true);
        $(".search-select-year option:contains(Year)").attr('selected', true);
        $('.search-keyword').val('');
        $(".search-button-search").trigger("click");
        return false;
    });


})( jQuery );

function update_search_count(data){
    var current_total = parseInt(document.getElementById("current_search_total_footer").innerHTML)+parseInt(data);
    document.getElementById("current_search_total_footer").innerHTML = current_total;
    document.getElementById("current_search_total_footer").innerHTML = current_total;
	document.getElementsByClassName("current_search_total_footer")[0].innerHTML = current_total;

    var progress = (current_total/ parseInt(document.getElementById("total_search_total_footer").innerHTML)) * 100;
    document.getElementById('search_progress').style.width = progress+"%";

    if(document.getElementById("current_search_total_footer").innerHTML==document.getElementById("total_search_total_footer").innerHTML){
        document.getElementById('search_load_more_posts').style.visibility = "hidden"; //visible
        document.getElementById('no_post').style.visibility = "visible";
    }
}