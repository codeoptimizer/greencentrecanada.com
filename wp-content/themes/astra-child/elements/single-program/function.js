jQuery(document).ready(function($) {
    var paged         = 1;
    var post_count    = 0;
    var sort       = 'ASC';
    var has_filter    = false;
    var category      = [];
    var selected      = [];

    if (stories.default_category !== 'all') {
        category = stories.default_category.split(':');
    }

    function do_filter() {

        $('.results-loader').addClass('loading');

        display_selection();

        $.ajax({
            url : stories.ajaxurl,
            type: 'post',
            data: {
                action      : 'stories_cpt',
                paged       : paged,
                sort        : sort,
                category    : category,
            }
        }).then(function(response) {


            post_count += parseInt(response.data.post_count);
            var start_count = post_count ? (paged - 1) * 9 : 0;
            post_count = post_count ? post_count : 0;

            if (post_count) {
                $('.found-posts').html( response.data.found_posts );
                $('.count-start').html( (paged === 1? post_count ? 1 : 0 : start_count) );
                $('.post-count').html( post_count );
                $('.results-count, .filter-load-more').show();
                $('.filter-no-postings').hide();

                $('.progress-label').html('Showing '+post_count+' of '+response.data.found_posts);
                $('.results-progress .progress span').css('width', ((paged/response.data.max_num_pages) * 100)+'%' );
            }
            else {
                $('.results-count, .filter-load-more').hide();
                $('.filter-no-postings').show();
            }

            if (response.data.found_posts <= 8) {
                $('.filter-load-more').hide();
                $('.filter-end-results').show();
            }else{
                $('.filter-end-results').hide();
            }


            if (paged === 1 )
                $('.filter-results').html( response.posts );
            else
                $('.filter-results').append( response.posts );

            $('.results-loader').removeClass('loading');
        }, function(){ // Reject!
            console.log('Something broke!');
        });
    }

    function display_selection() {
        var term_selected = '';
        $.each(selected, function(index, item) {
            term_selected += `
            <span class="term-selected" data-id="`+item.term_id+`" data-taxonomy="`+item.taxonomy+`">x `+ item.name +`</span>
            `;
        });

        $('.applied-filters').html(term_selected);
    }

    function reset_taxonomy() {
        post_count    = 0;
        selected      = [];
        category   = [];
        has_filter    = false;
    }


    function taxonomy(callback) {
        reset_taxonomy();

        $('.filter-item').each(function() {

            $input = $(this).find('.sort');
            if ($input.hasClass('selected')) {
                sort = $input.data('sort');
            }

            $input = $(this).find('.term-input');
            if ($input.hasClass('selected')) {
                has_filter = true;

                var taxonomy = $input.data('taxonomy');
                var term_id  = $input.data('id');
                var name     = $input.data('name');

                selected.push({
                    taxonomy: taxonomy,
                    term_id : term_id,
                    name    : name
                });

                if (taxonomy === 'category') {
                    category.push( term_id );
                }
            }
        });
        callback();
    }

	// Load more
   $(document).on('click', '.filter-load-more a', function(e) {
       e.preventDefault();
        paged += 1;
        do_filter();
    });

    // Filters
    $(document).on('click', '.filter-button', function(e) {
        e.preventDefault();

        if(!e.target.classList.contains("dropdown-container") && $(e.target).parents('.dropdown-container').length==0){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
            }else{
                $('.filter-button').removeClass('active');
                $(this).addClass('active');
            }
        }

    });

    $(document).on('click', '.filter-item .term-input', function(e) {
        e.preventDefault();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            $(this).addClass('selected');
        }

        taxonomy(function() {
            paged = 1;
            do_filter();
        });

    });

    $(document).on('click', '.filter-item .sort', function(e) {
        e.preventDefault();

        $input = $(this);

         $('.filter-item .sort').removeClass('selected');

        if ($input.hasClass('selected')) {
            $input.removeClass('selected');
        } else {
            $input.addClass('selected');
        }

        taxonomy(function() {
            paged = 1;
          do_filter();
        });

    });



    // All
    $(document).on('click', '#filter-all', function(e) {
        e.preventDefault();
        reset_taxonomy();
        $('.filter-item .term-input').removeClass('selected');
        paged = 1;
        do_filter();
    });

    // Remove term selected
    $(document).on('click', '.term-selected', function(e) {
        e.preventDefault();
            $term    = $(this);
        var _taxonomy = $term.data('taxonomy');
        var _term_id  = $term.data('id');

        $('.term-input[data-taxonomy="'+_taxonomy+'"]').each(function() {
            $input = $(this);
            var id = $input.data('id');
            if (id === _term_id) {
                $input.removeClass('selected');
            }
        });
        taxonomy(function() {
            paged = 1;
            do_filter();
        });
    });

    // hide dropdown when click outside the wrapper
    $(document).on('click', function (e) {
        if ($(e.target).closest(".filter-button").length === 0 && $(e.target).closest(".daterangepicker").length === 0 ) {
           $(".filter-button").removeClass('active');
        }
    });

    setInterval(function() {
        if (has_filter) {
            $('#filter-all').removeClass('selected');
        } else {
            $('#filter-all').addClass('selected');
        }

        if ($('.daterangepicker').is(':visible')) {
            $('#filter-date').addClass('active');
        } else {
            $('#filter-date').removeClass('active');
        }

    }, 100);


    // Load default
    taxonomy(function() {
        paged = 1;
        do_filter();
    });


    /* Custom Slider Next/Prev trigger */

    $(".elementor-swiper:nth-child(2)").prepend("<span class='prev-button'></span>");
    $(".elementor-swiper:nth-child(2)").append("<span class='next-button'></span>");


    $('body').on('click','.elementor-swiper:nth-child(2) .prev-button',function() {
        const swiper_target = jQuery ( '.swiper-container' );
        const swiperInstance = swiper_target.data('swiper');
        swiperInstance.slidePrev();
    });

    $('body').on('click','.elementor-swiper:nth-child(2) .next-button',function() {
        const swiper_target = jQuery ( '.swiper-container' );
        const swiperInstance = swiper_target.data('swiper');
        swiperInstance.slideNext();
    });

    $('body').on('click','.elementor-swiper:nth-child(2)',function() {

        const swiper_target = jQuery ( '.swiper-container' );
        const swiperInstance = swiper_target.data('swiper');

        //returns true/false
        if(psuedoClick(this).before){
            swiperInstance.slidePrev();
        }

        //returns true/false
        if(psuedoClick(this).after){
            swiperInstance.slideNext();
        }

        return false;

    });

    function psuedoClick(parentElem) {

        var beforeClicked,
        afterClicked;

        var parentLeft = parseInt(parentElem.getBoundingClientRect().left, 10),
            parentTop = parseInt(parentElem.getBoundingClientRect().top, 10);

        var parentWidth = parseInt(window.getComputedStyle(parentElem).width, 10),
            parentHeight = parseInt(window.getComputedStyle(parentElem).height, 10);

        var before = window.getComputedStyle(parentElem, ':before');

        var beforeStart = parentLeft + (parseInt(before.getPropertyValue("left"), 10)),
            beforeEnd = beforeStart + parseInt(before.width, 10);

        var beforeYStart = parentTop + (parseInt(before.getPropertyValue("top"), 10)),
            beforeYEnd = beforeYStart + parseInt(before.height, 10);

        var after = window.getComputedStyle(parentElem, ':after');

        var afterStart = parentLeft + (parseInt(after.getPropertyValue("left"), 10)),
            afterEnd = afterStart + parseInt(after.width, 10);

        var afterYStart = parentTop + (parseInt(after.getPropertyValue("top"), 10)),
            afterYEnd = afterYStart + parseInt(after.height, 10);

        var mouseX = event.clientX,
            mouseY = event.clientY;

        beforeClicked = (mouseX >= beforeStart && mouseX <= beforeEnd && mouseY >= beforeYStart && mouseY <= beforeYEnd ? true : false);

        afterClicked = (mouseX >= afterStart && mouseX <= afterEnd && mouseY >= afterYStart && mouseY <= afterYEnd ? true : false);

        return {
            "before" : beforeClicked,
            "after"  : afterClicked

        };
    }
    /* End custom Slider Next/Prev trigger */

});

