<?php
/*Load js and style in CPT only*/

add_action( 'wp_enqueue_scripts', function() {
    global $wp_query;
    $post_id = $wp_query->post->ID;
    $post = get_post( $post_id );
    $slug = $post->post_name;

    if ( $slug == 'programs' ) {
        wp_enqueue_style( 'stories-style', get_stylesheet_directory_uri() . '/elements/single-program/style.css');
        wp_enqueue_script('stories-script', get_stylesheet_directory_uri() . '/elements/single-program/stories.js',  array ('jquery'), false, false);

        wp_localize_script(
            'stories-script',
            'stories',
            array(
                'ajaxurl' => admin_url( 'admin-ajax.php' ),
                'default_category' => get_query_var('category', 'all'),
            )
        );
    }
});


# Ajax
add_action( 'wp_ajax_stories_cpt', 'stories_cpt' );
add_action( 'wp_ajax_nopriv_stories_cpt', 'stories_cpt' );
function stories_cpt() {

    ob_start();

    $paged         = $_POST['paged'];
    $category      = $_POST['category'];
    $sort       = $_POST['sort'];
    $tax_query     = [];
    $meta_query    = [];

    $args = array(
        'post_type'      => 'story',
        'post_status'    => 'publish',
        'posts_per_page' => 6,
        'paged'          => $paged,
        'order' => $sort ,
    );



    # Category
    if (count($category)) {
        $tax_query[] = array(
            'taxonomy' => 'category',
            'field'    => 'term_id',
            'terms'    => $category,
            'compare'  => 'in'
        );
    }


    # Tax query
    if (count($tax_query))
        $args['tax_query'] = $tax_query;

    $results = new WP_Query($args);

    if ($results->have_posts()): while ($results->have_posts()): $results->the_post();
        $src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );

        echo '<div class="loop-wrapper">';
        echo '<a href="'.get_permalink().'"></a>';
        echo do_shortcode('[elementor-template id="3887"]');
        echo '</div>';
    endwhile; endif;


    wp_send_json([
        'posts' => ob_get_clean(),
        'data'  => $results,
        'meta'  => $args['meta_query'],
        'tax'   => $tax_query
    ]);


    wp_die();
}


# Filters
add_shortcode( 'stories-filter', function() {
    ob_start();
    include ('filter/dropdown-button.php');
    return ob_get_clean();
} );

# Filters
add_shortcode( 'stories-filter-results', function() {
    ob_start();
    echo '<div class="filter-results-wrapper results-loader"><div class="filter-results"></div></div>';
    return ob_get_clean();
} );


function load_past_recipients() {
    ob_start();

    if( have_rows('past_recipients') ) :
        while ( have_rows('past_recipients')) :
            the_row();
            $recipient_title = get_sub_field('recipient_title');
            $recipient_companies = get_sub_field('recipient_companies');
            ?>
            <section class="elementor-section elementor-inner-section elementor-element elementor-element-4d0838a elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="4d0838a" data-element_type="section">
                <div class="elementor-container elementor-column-gap-no">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ad2124d" data-id="ad2124d" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-9811c8a elementor-widget elementor-widget-heading" data-id="9811c8a" data-element_type="widget" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                    <?php if(isset($recipient_title)): ?>
                                        <h5 class="elementor-heading-title elementor-size-default"><?php echo $recipient_title; ?></h5>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-6ada13a elementor-widget elementor-widget-heading" data-id="6ada13a" data-element_type="widget" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                    <?php
                                        if(isset($recipient_companies)):
                                            echo $recipient_companies;
                                        endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php
        endwhile;
    endif;
    return ob_get_clean();
}
add_shortcode('load_past_recipients', 'load_past_recipients');

function load_testimonials() {
    ob_start();

    if( have_rows('testimonials') ) :
        $row = 1;
        while ( have_rows('testimonials')) :
            the_row();
            $quote = get_sub_field('quote');
            $attribution_title = get_sub_field('attribution_title');
            $attribution_position = get_sub_field('attribution_position');
            ?>
            <div id="testimonial-<?= $row; ?>" class="elementor-testimonial">
                <div class="elementor-testimonial__content">
                    <div class="elementor-testimonial__text">
                        <?= $quote; ?>
                    </div>
                    <cite class="elementor-testimonial__cite">
                        <span class="elementor-testimonial__name"><?= $attribution_title; ?></span>
                        <span class="elementor-testimonial__title"><?= $attribution_position; ?></span>
                    </cite>
                </div>
            </div>
        <?php
            $row++;
        endwhile;
    endif;
    return ob_get_clean();
}
add_shortcode('load_testimonials', 'load_testimonials');