<?php
define ('THEME_DIR', get_stylesheet_directory());
define ('THEME_URI', get_stylesheet_directory_uri());

//* Enqueue Scripts
add_action( 'wp_enqueue_scripts', function() {
   wp_enqueue_style('custom-global-style', THEME_URI.'/elements/global-style.css');
   wp_enqueue_script('custom-global-script', THEME_URI . '/elements/global-script.js', array ('jquery'), false, false);
});


require_once('news/functions.php');
require_once('single-news/functions.php');
require_once('program/functions.php');
require_once('single-program/functions.php');


# Terms
add_shortcode( 'article-terms', function($atts) {
    $terms = '';
    $args = shortcode_atts( array(
        'tax' => 'category',
        'sector' => 'null'
    ), $atts );

    $image = get_the_post_thumbnail_url();
    $categories = get_the_terms(get_the_ID(), $args['tax']) ?? '';
    $sectors = get_the_terms(get_the_ID(), $args['sector']) ?? '';
    if (count($categories)) {
        foreach ( $categories as $cat ) {
            $terms .= '<span class="article-term term-'.$cat->slug.'">'.$cat->name.'</span>';
        }
    }
    if (count($sectors)) {
        foreach ( $sectors as $sector ) {
            $terms .= '<span class="article-term term-'.$sector->slug.'">'.$sector->name.'</span>';
        }
    }
    return '<div class="article-terms">'.$terms.'</div>';
} );

# excerpt
add_shortcode( 'post-excerpt', function($atts) {
    $atts=shortcode_atts(
    array(
        'limit' => 10,
    ), $atts);

   return wp_trim_words(get_the_excerpt(), $atts['limit'],'');
} );

//excerpt post title
add_shortcode('title-excerpt', 'title_excerpt_function');

function title_excerpt_function($atts){

   shortcode_atts(array(
      'limit' => 1,
	  'type' => 'words',
   ), $atts);

	if($atts['type']=='words'){
		$title = wp_trim_words( get_the_title(), $atts['limit'], '' );
	}else if($atts['type']=='character'){
		$title = substr(get_the_title(), 0, $atts['limit']);
	}


   //wp_reset_query();
   return $title;
}