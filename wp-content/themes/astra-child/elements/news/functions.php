<?php
# Library
add_action('wp_enqueue_scripts', function() {
    wp_register_style(
		'news-css',
		THEME_URI . '/elements/news/style.css',
		array(),
		uniqid(),
		'all'
	);
    wp_register_style(
		'news-single-css',
		THEME_URI . '/elements/news/single.css',
		array(),
		uniqid(),
		'all'
	);

    wp_register_script(
		'news-script',
		THEME_URI . '/elements/news/function.js',
		array('jquery'),
		uniqid().
		true
	);
    wp_localize_script(
        'news-script',
        'gc',
        array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        )
    );


    # Load singe news library
    if (is_singular('post')) {
        wp_enqueue_style('news-single-css');
    }
});


# Loop item
add_action('news-loop', function() {
    include THEME_DIR . '/elements/news/loop.php';
});


# Ajax
add_action('wp_ajax_news-feeds', 'gc_news_feeds');
add_action('wp_ajax_nopriv_news-feeds', 'gc_news_feeds');
function gc_news_feeds() {
    ob_start();

    $paged = $_POST['paged'];

    $results = new WP_Query(array(
        'post_type'      => 'post',
        'post_status'    => 'publish',
        'posts_per_page' => 8,
        'paged'          => $paged
    ));

    if ($results->have_posts()): while ($results->have_posts()): $results->the_post();
        do_action('news-loop');
    endwhile; else: echo '<div class="no-results">'.($paged > 1?'No more post found.':'No results found.').'</div>'; endif;

    wp_send_json(array(
        'news' => ob_get_clean(),
        'data' => $results,
    ));
    wp_die();
}


# Load result container
add_shortcode('news-feeds', function($atts = array()) {
    ob_start();
    wp_enqueue_style('news-css');
    wp_enqueue_script('news-script');
    ?>
    <div class="news-ajax">
        <div class="news-feeds"></div>
        <div class="news-load-more">
            <div class="results-progress">
                <span class="progress-label"></span>
                <span class="progress">
                    <span style="width: 16.6667%;"></span>
                </span>
            </div>
            <a href="#" class="load-more">
                <span>Load More</span>
            </a>
        </div>
    </div>
    <?php
    return ob_get_clean();
});

# File download
add_shortcode('news-file-download', function() {
    ob_start();
    $file = get_field('file_download');

	if(!empty($file['button_url']) && !empty($file['button_text']) && !empty($file['file_name'])):
    	include THEME_DIR . '/elements/news/file-download.php';
	endif;
    return ob_get_clean();
});

# Contact
add_shortcode('news-contact', function() {
    ob_start();
    $contact = get_field('contacts');
	
	if(!empty($contact['button_url']) && !empty($contact['button_text'])):
	    include THEME_DIR . '/elements/news/contact.php';
	endif;
    return ob_get_clean();
});

# Related news
add_shortcode('news-related', function() {
    ob_start();

    $results = new WP_Query(array(
        'post_type'      => 'post',
        'post_status'    => 'publish',
        'posts_per_page' => 6,
        'post__not_in'   => array(get_the_ID())
    ));

    echo '<div class="news-related-items">';
    if ($results->have_posts()): while ($results->have_posts()): $results->the_post();
        do_action('news-loop');
    endwhile; endif;
    wp_reset_query();
    echo '</div>';
    ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script>
        jQuery(document).ready(function($) {
            console.log('Initializing related news...');

            var slider;

            var settings = function() {
                var settings1 = {
                    slideWidth    : $(window).width() - 60,
                    minSlides     : 1,
                    maxSlides     : 1,
                    slideMargin   : 0,
                    responsive    : true,
                    pager         : false,
                    adaptiveHeight: true,
                    moveSlides    : 1,
                    startSlide    : 0,
                    captions      : false,

                };
                var settings2 = {
                    slideWidth    : 557,
                    minSlides     : 2,
                    maxSlides     : 2,
                    slideMargin   : 25,
                    responsive    : true,
                    pager         : false,
                    adaptiveHeight: true,
                    moveSlides    : 1,
                    startSlide    : 0,
                    captions      : false,
                };
                return ($(window).width()<=767) ? settings1 : settings2;
            }

            $(window).on('resize', function() {
                slider.reloadSlider(settings());
            });

            slider = $(".news-related-items").bxSlider(settings());


            setInterval(function() {
                if ($('.bx-next svg').length < 1) {
                    $('.bx-next').html(
                        `<svg id="Group_22782" data-name="Group 22782" xmlns="http://www.w3.org/2000/svg" width="47.204" height="47.204" viewBox="0 0 47.204 47.204">
                        <g id="Ellipse_630" data-name="Ellipse 630" fill="#fff" stroke="#0f0f0f" stroke-width="2">
                            <ellipse cx="23.602" cy="23.602" rx="23.602" ry="23.602" stroke="none"/>
                            <ellipse cx="23.602" cy="23.602" rx="22.602" ry="22.602" fill="none"/>
                        </g>
                        <g id="Controls_Slider_Arrow_Left" data-name="Controls / Slider Arrow / Left" transform="translate(28.537 33.069) rotate(180)">
                            <rect id="Controls_Slider_Arrow_Left_background" data-name="Controls / Slider Arrow / Left background" width="10.535" height="21.069" transform="translate(0.001)" fill="none"/>
                            <path id="Line" d="M0,0,6.7,6.7l6.7-6.7" transform="translate(6.695 2.504) rotate(90)" fill="none" stroke="#0f0f0f" stroke-linecap="square" stroke-width="2"/>
                        </g>
                        </svg>
`
                    );
                }
                if ($('.bx-prev svg').length < 1) {
                    $('.bx-prev').html(
                        `<svg xmlns="http://www.w3.org/2000/svg" width="47.204" height="47.204" viewBox="0 0 47.204 47.204">
                        <g id="Group_22783" data-name="Group 22783" transform="translate(47.204 47.204) rotate(180)">
                            <g id="Ellipse_630" data-name="Ellipse 630" fill="#fff" stroke="#0f0f0f" stroke-width="2">
                            <ellipse cx="23.602" cy="23.602" rx="23.602" ry="23.602" stroke="none"/>
                            <ellipse cx="23.602" cy="23.602" rx="22.602" ry="22.602" fill="none"/>
                            </g>
                            <g id="Controls_Slider_Arrow_Left" data-name="Controls / Slider Arrow / Left" transform="translate(28.537 33.069) rotate(180)">
                            <rect id="Controls_Slider_Arrow_Left_background" data-name="Controls / Slider Arrow / Left background" width="10.535" height="21.069" transform="translate(0.001)" fill="none"/>
                            <path id="Line" d="M0,0,6.7,6.7l6.7-6.7" transform="translate(6.695 2.504) rotate(90)" fill="none" stroke="#0f0f0f" stroke-linecap="square" stroke-width="2"/>
                            </g>
                        </g>
                        </svg>
`
                    );
                }
            }, 100);
        });
    </script>

    <?php

    return ob_get_clean();
});


# Copy to clipboard
add_shortcode( 'copy_clipboard', function() {
    ob_start();
    ?>
    <div class="clipboard">
        <input onclick="copy()" class="copy-input" value="<?php echo get_permalink(); ?>" id="copyClipboard" readonly>
    </div>

    <div id="copied-success">Copied!</div>

    <script>
        function copy() {
            /* Get the text field */
            var el_copied = document.getElementById("copied-success");
            var copyText = document.getElementById("copyClipboard");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */

            navigator.clipboard.writeText(copyText.value);
            el_copied.classList.add("copied");
            el_copied.style.opacity = '1';

            setTimeout(() => {
                el_copied.style.opacity = '0';
            }, 2000);
        }
        </script>
        <?php
    return ob_get_clean();
} );