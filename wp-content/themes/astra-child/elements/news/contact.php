<div class="news-contact">
    <div class="grid">
        <div class="contact-details">
            <h3 class="title">Contacts</h3>
            <div class="contact-name"><?php echo $contact['contact_name'] ?></div>
            <div class="job-role"><?php echo $contact['contact_job_role'] ?></div>
        </div>
        <div class="cta">
            <a href="<?php echo $contact['button_url'] ?>">
                <span><?php echo $contact['button_text'] ?></span>
            </a>
        </div>
    </div>
</div>