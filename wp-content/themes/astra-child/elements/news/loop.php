<?php
$excerpt = get_the_excerpt();
$excerpt = substr($excerpt, 0, 120);
$excerpt = substr($excerpt, 0, strrpos($excerpt, ' '));

?>
<article id="news-loop-item-<?php the_ID() ?>" class="news-loop-item news-loop-item-<?php the_ID() ?>">
    <a href="<?php the_permalink() ?>" class="cta-link"></a>
    <div class="article-container">

        <?php
        $categories = get_the_category();
        if ( ! empty( $categories ) ): ?>
        <div class="terms">
            <?php foreach($categories as $cat): ?>
            <span class="term-item"><?php echo $cat->name ?></span>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>

        <h3><?php the_title() ?></h3>
        <div class="date"><?php echo get_the_date('F j, Y'); ?></div>
        <div class="excerpt"><?php echo $excerpt ?></div>
        <div class="cta">
            <a href="<?php the_permalink() ?>">
                <span>View Program</span>
                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
                    <g id="Group_23516" data-name="Group 23516" transform="translate(-115 -1)">
                        <g id="Ellipse_133" data-name="Ellipse 133" transform="translate(115 1)" fill="#fff" stroke="#0f0f0f" stroke-width="1">
                        <circle cx="11" cy="11" r="11" stroke="none"/>
                        <circle cx="11" cy="11" r="10.5" fill="none"/>
                        </g>
                        <g id="arrow-up" transform="translate(129.053 9.426) rotate(90)">
                        <line id="Line_38" data-name="Line 38" y1="7.092" transform="translate(2.58 0.755)" fill="none" stroke="#0f0f0f" stroke-linejoin="round" stroke-width="1"/>
                        <path id="Path_5971" data-name="Path 5971" d="M0,2.816,2.575,0,5.15,2.816" fill="none" stroke="#0f0f0f" stroke-width="1"/>
                        </g>
                    </g>
                </svg>
            </a>
        </div>
    </div>
</article>