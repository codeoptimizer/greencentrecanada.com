jQuery(document).ready(function($) {

    var paged = 1;
    var post_count = 0;

    function do_query() {
        $('.news-ajax').addClass('loading');

        $.ajax({
            url     : gc.ajaxurl,
            type    : 'POST',
            dataType: 'json',
            data    : {
                action: 'news-feeds',
                paged : paged
            }
        }).done(function(response) {

            post_count += parseInt(response.data.post_count);
            var start_count = post_count ? (paged - 1) * 13 : 0;
            post_count = post_count ? post_count : 0;

            if (post_count) {
                $('.results-count, .news-load-more').show();
                $('.no-results').hide();

                $('.progress-label').html('Showing '+post_count+' of '+response.data.found_posts);
                $('.results-progress .progress span').css('width', ((paged/response.data.max_num_pages) * 100)+'%' );
            }
            else {
                $('.results-count, .news-load-more').hide();
                $('.no-results').show();
            }


            if (response.data.found_posts < 8 || post_count === response.data.found_posts || post_count == 0) {
                $('.no-results').show();
                $('.news-load-more').hide();
            }


            if (paged > 1) {
                $('.news-feeds').append(response.news);
            } else {
                $('.news-feeds').html(response.news);
            }

            $('.news-ajax').removeClass('loading');
        });
    }


    $(document).on('click', '.load-more', function(e) {
        e.preventDefault();
        paged += 1;
        do_query();
    });


    do_query();
});