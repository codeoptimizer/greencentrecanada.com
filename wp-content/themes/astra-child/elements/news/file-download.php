<div class="file-download">
    <div class="icon">
        <img src="<?php echo $file['icon'] ?>" alt="">
    </div>
    <div class="file-details">
        <div class="file-subname"><?php echo $file['file_sub_name'] ?></div>
        <div class="file-name"><?php echo $file['file_name'] ?></div>
        <div class="cta">
            <a href="<?php echo $file['button_url'] ?>">
                <span><?php echo $file['button_text'] ?></span>
                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
                    <g id="Group_23516" data-name="Group 23516" transform="translate(-115 -1)">
                        <g id="Ellipse_133" data-name="Ellipse 133" transform="translate(115 1)" fill="#fff" stroke="#0f0f0f" stroke-width="1">
                        <circle cx="11" cy="11" r="11" stroke="none"/>
                        <circle cx="11" cy="11" r="10.5" fill="none"/>
                        </g>
                        <g id="arrow-up" transform="translate(129.053 9.426) rotate(90)">
                        <line id="Line_38" data-name="Line 38" y1="7.092" transform="translate(2.58 0.755)" fill="none" stroke="#0f0f0f" stroke-linejoin="round" stroke-width="1"/>
                        <path id="Path_5971" data-name="Path 5971" d="M0,2.816,2.575,0,5.15,2.816" fill="none" stroke="#0f0f0f" stroke-width="1"/>
                        </g>
                    </g>
                </svg>
            </a>
        </div>
    </div>
</div>