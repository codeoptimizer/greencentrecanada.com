<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

require_once('elements/global-functions.php');

/**
 * Enqueue styles
 */
function child_enqueue_styles() {
	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
}
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );


add_filter( 'astra_tablet_breakpoint', function() {
    return 1025;
});


function team_modify_featured_image_labels( $labels ) {	
   
	$labels->featured_image        = __( 'Profile Picture', 'textdomain' );
	$labels->set_featured_image    = __( 'Set Profile Picture', 'textdomain' );
	$labels->remove_featured_image = __( 'Remove Profile Picture', 'textdomain' );
	$labels->use_featured_image    = __( 'Use as Profile Picture', 'textdomain' );

	return $labels;
}
add_filter( 'post_type_labels_team-bio', 'team_modify_featured_image_labels', 10, 1 );


/**
 * Create post type function
 *----------------------------*/
function _create_post_type($slug, $singular, $plural, $icon, $rw_slug='') {
	register_post_type( $slug, array(
		'labels' => array(
			'name'               => __($plural, 'dxbase'),
			'singular_name'      => __($singular, 'dxbase'),
			'add_new'            => _x("Add New", 'pluginbase', 'dxbase' ),
			'add_new_item'       => __("Add New ".$singular, 'dxbase' ),
			'edit_item'          => __("Edit ".$singular, 'dxbase' ),
			'new_item'           => __("New ".$singular, 'dxbase' ),
			'view_item'          => __("View ".$singular, 'dxbase' ),
			'search_items'       => __("Search ".$plural, 'dxbase' ),
			'not_found'          => __("No ".$plural." found", 'dxbase' ),
			'not_found_in_trash' => __("No ".$plural." found in Trash", 'dxbase' ),
		),
		'description'        => __($singular, 'dxbase'),
		'public'             => true,
		'publicly_queryable' => true,
		'query_var'          => true,
		'exclude_from_search' => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 40,
		'show_in_rest'        => true,
		'supports'            => array(
			'title',
			'thumbnail',
			'editor',
			'excerpt'
		),
		'menu_icon' => 'dashicons-'.$icon,
		'rewrite' => array('slug' => $rw_slug),
	));
}

/**
 * Create taxonomy function
 *----------------------------------*/
function _post_type_taxonomy($slug,$singular,$plural,$postype){

	// Add new taxonomy for professions
	$labels = array(
		'name'              => _x($plural, 'taxonomy general name', 'textdomain'),
		'singular_name'     => _x('Category', 'taxonomy singular name', 'textdomain'),
		'search_items'      => __('Search '.$plural, 'textdomain'),
		'all_items'         => __('All '.$plural, 'textdomain'),
		'parent_item'       => __('Parent '.$singular, 'textdomain'),
		'parent_item_colon' => __('Parent '.$singular.':', 'textdomain'),
		'edit_item'         => __('Edit '.$singular, 'textdomain'),
		'update_item'       => __('Update '.$singular, 'textdomain'),
		'add_new_item'      => __('Add New '.$singular, 'textdomain'),
		'new_item_name'     => __('New '.$singular.' Name', 'textdomain'),
		'menu_name'         => __($plural, 'textdomain'),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'show_in_rest'      => true,
	);

	register_taxonomy($slug, array($postype), $args);
}
add_action('init','_render_post_type');
function _render_post_type(){

// 	_create_post_type('team-bio','Team Bio','Team Bios','businessman','team-bio');
// 	_post_type_taxonomy('team-categories','Category','Categories','team-bio');

}




function hook_analytics() {
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2LC118F612"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-2LC118F612');
</script>
<?php
}
add_action('wp_head', 'hook_analytics');


add_action('wp_footer', '_footer_hooks_bio');
function _footer_hooks_bio() {
	?>
	<script>
	jQuery(document).ready(function($) {

		$('.profile-bio').each(function() {
			var _link = $(this).find('a');
			if (_link.length < 1) {
				$(this).hide();
				$(this).parent().addClass('no-bio');
			} else {
				$(this).show();
			}
		});
		$('.profile-email').each(function() {
			var _link = $(this).find('a');
			if (_link.length < 1) {
				$(this).hide();
				$(this).parent().addClass('no-email');
			} else {
				$(this).show();

				var _mail = _link.attr('href');
				_link.attr('href', _mail.replace('http://', 'mailto:'));
			}
		});
		$('.single-team-email').each(function() {
			var _link = $(this).find('a');
			if (_link.length < 1) {
				$(this).hide();
				$(this).parent().addClass('no-email');
			} else {
				$(this).show();

				var _mail = _link.attr('href');
				_link.attr('href', _mail.replace('http://', 'mailto:'));
			}
		});
		$('.single-team-phone').each(function() {

			$container = $(this).find('.elementor-heading-title');
			if ($container.length) {
				$(this).show();
				$(this).prev().show();

				var _phone = $container.html().toString();							
				var _arr   = _phone.replace(/\s/g, '').split('<br>');
				var _links = '';
				
				for (var index = 0; index < _arr.length; index++) {

					var _text = _arr[index];
					var _link = (_text.split('.').join('')).split('x');

					_links += `<a href="tel:+1`+_link[0]+`">`+_text+`</a>`;
				}

				$container.html(_links);
			}
		});
	});
	</script>
	<?php
}